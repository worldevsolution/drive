<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login()
    {
        try{
            $endpoint = 'http://127.0.0.1:8000/api';
            $client = new \GuzzleHttp\Client();
            // Create a request
            $request = $client->get($endpoint.'/login');
            // Get the actual response without headers
            $response = $request->getBody();
            $json_decode=json_decode($response,true);
            foreach ($json_decode as $key => $value) {
                echo $value['title'].'<br>';
            }

        }catch(\Exception $e){
            return $e->getMessage().' '. $e->getLine() .' '. $e->getFile();

        }
    }
}
