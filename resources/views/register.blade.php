@include('header')
<style>
    .site-header{
        background: #000;
    }
    .main-navigation > li > a,.sign-in{
        color: #fff;
    }
    .main-navigation > li > a:hover,.main-navigation > li > a.active,.sign-in:hover{
        color: #00a0e1;
    }
    .page-content{
        padding: 50px 0px;
    }
</style>
<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <div class="form-login js-login-form">
                    <div class="form-login__block js-form-block" id="signup">
                        <form>
                            <div class="form-container">
                                <h3 class="form-title t-center">Créer un compte</h3>
                                <div class="form-social">
                                    <div class="form-group">
                                        <div class="form-group__wrapper">
                                            <button
                                                    class="button button--social button--twitter button--pill button--large button--block"
                                                    type="button"
                                            >
                                                Se connecter avec Twitter
                                            </button>
                                            <span class="form-group__icon form-group__icon--social">
                                                <i class="fa fa-twitter c-white"></i>
                                            </span>
                                        </div><!-- .form-group__wrapper -->
                                    </div><!-- .form-group -->

                                    <div class="form-group">
                                        <div class="form-group__wrapper">
                                            <button
                                                    class="button button--social button--facebook button--pill button--large button--block"
                                                    type="button"
                                            >
                                                Se connecter avec Facebook
                                            </button>
                                            <span class="form-group__icon form-group__icon--social">
                                                <i class="fa fa-facebook-f c-white"></i>
                                            </span>
                                        </div><!-- .form-group__wrapper -->
                                    </div><!-- .form-group -->
                                </div><!-- .form-social -->

                                <div class="form-group">
                                    <label for="signup-name">Nom *</label>
                                    <input
                                            type="text"
                                            name="nom"
                                            id=""
                                            class="form-input form-input--pill form-input--border-c-gallery"
                                            required
                                            placeholder="John Doe"
                                    >
                                </div><!-- .form-group -->

                                <div class="form-group">
                                    <label for="signup-name">Prénom</label>
                                    <input
                                            type="text"
                                            name="prenom"
                                            id=""
                                            class="form-input form-input--pill form-input--border-c-gallery"
                                            required
                                            placeholder="John Doe"
                                    >
                                </div><!-- .form-group -->

                                <div class="form-group">
                                    <label for="">Email *</label>
                                    <input
                                            type="email"
                                            name="email"
                                            id=""
                                            class="form-input form-input--pill form-input--border-c-gallery"
                                            required
                                            placeholder=""
                                    >
                                </div><!-- .form-group -->

                                <div class="form-group">
                                    <label for="">Téléphone *</label>
                                    <input
                                            type="text"
                                            name="telephone"
                                            id=""
                                            class="form-input form-input--pill form-input--border-c-gallery"
                                            required
                                            placeholder=""
                                    >
                                </div><!-- .form-group -->

                                <div class="form-group">
                                    <label for="">Adresse </label>
                                    <input
                                            type="text"
                                            name="adresse"
                                            id=""
                                            class="form-input form-input--pill form-input--border-c-gallery"
                                            required
                                            placeholder=""
                                    >
                                </div><!-- .form-group -->

                                <div class="form-group">
                                    <label for="signup-password">Mot de passe *</label>
                                    <input
                                            type="password"
                                            name="signup-password"
                                            id="signup-password"
                                            class="form-input form-input--pill form-input--border-c-gallery"
                                            required
                                            placeholder="******"
                                    >
                                </div><!-- .form-group -->

                                <div class="form-group">
                                  <span class="c-gray">
                                    En créant votre compte, vous acceptez nos <a href="#" class="t-underline">Termes et Conditions</a>
                                    et notre <a href="#" class="t-underline">Politique de confidentialité</a>
                                  </span>
                                </div><!-- .form-group -->

                                <div class="form-group--submit">
                                    <button
                                            class="button button--primary button--pill button--large button--block button--submit"
                                            type="submit"
                                    >
                                        Enregistrer
                                    </button>
                                </div>

                                <div class="form-group--footer">
                                  <span class="c-gray">
                                    Vous avez déjà un compte?
                                    <a href="login" class="">Se connecter</a>
                                  </span>
                                </div>
                            </div><!-- .form-container -->
                        </form><!-- .signup -->
                    </div><!-- .form-login__block -->

                </div><!-- .form-login -->
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .page-content -->
@include('footer')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4"></script>
<script src="../../../../cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
<script src="assets/scripts/app.js"></script>
</body>

<!-- Mirrored from haintheme.com/demo/html/listiry/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Oct 2018 14:59:02 GMT -->
</html>
