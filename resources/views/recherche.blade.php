@include('header')
<style>
    .site-header{
        background: #000;
    }
    .main-navigation > li > a,.sign-in{
        color: #fff;
    }
    .main-navigation > li > a:hover,.main-navigation > li > a.active,.sign-in:hover{
        color: #00a0e1;
    }
    .page-content{
        padding: 50px 0px;
    }
</style>
<div class="page-content">
    <div class="container">
        <div class="row" style="margin-top: 30px;">
            <div class="col-md-12">
                <h5>Je recherche une place libre</h5>
            </div>
            <div class="row" style="width: 100%">
                <div class="col-md-4">
                    <select class="form-input">
                        <option>Départ</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <select class="form-input">
                        <option>Destination</option>
                    </select>
                </div>
                <div class="col-md-2" style="padding-left: 0px">
                    <input type="date" name="location" id="main-search-location" class="form-input" placeholder="Date">
                </div>
                <div class="col-md-2" style="padding: 0px">
                    <button style="width: 100%" type="submit" class="button button--medium button--square button--primary">
                        Rechercher
                    </button>
                </div>
            </div>
            <div class="row" style="margin-top: 30px;width: 100%">
                <center style="width: 100%;">
                    <img src="images/search.svg" style="width: 60%">
                </center>
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .page-content -->
<footer id="colophone" class="site-footer">
    <img src="images/footer_header1.png" />
    <div class="t-center site-footer__primary">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <ul class="Footer-menu">
                        <li class="Footer-item"><a href="/qui-sommes-nous" class="Footer-link">Qui sommes-nous ?</a></li>
                        <li class="Footer-item"><a href="/emploi" class="Footer-link">Rejoignez notre équipe</a></li>
                        <li class="Footer-item"><a href="/entreprises-collectivites" class="Footer-link">Nos partenaires</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="Footer-menu">
                        <li class="Footer-item"><a href="/faq" class="Footer-link">Aide / FAQ</a></li>
                        <li class="Footer-item"><a href="https://123envoiture.zendesk.com/hc/fr/requests/new" class="Footer-link">Contactez-nous</a>
                        </li>
                        <li class="Footer-item Footer-item--last">
                            <a href="https://lp.idvroom.com/bonne-id/prog-fid.html" class="Footer-link">La Bonne iD</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="Footer-menu">
                        <li class="Footer-item"><a href="/conditions-generales-utilisation" class="Footer-link">Conditions générales</a></li>
                        <li class="Footer-item"><a href="/mentions-legales" class="Footer-link">Mentions légales</a></li>
                        <li class="Footer-item"><a href="/charte-de-confidentialite" class="Footer-link">Charte de confidentialité</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- .site-footer__primary -->

    <div class="site-footer__secondary">
        <div class="container">
            <div class="site-footer__secondary-wrapper">
                <p class="site-footer__copyright">&copy; 2018
                    <span class="c-secondary">Drive</span> by worldevs. All Rights Reserved.</p>
                <ul class="min-list inline-list site-footer__links site-footer__social">
                    <li>
                        <a href="#">
                            <i class="fa fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-pinterest"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-youtube"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- .site-footer__secondary -->
</footer><!-- #colophone -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4"></script>
<script src="../../../../cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
<script src="assets/scripts/app.js"></script>
</body>

<!-- Mirrored from haintheme.com/demo/html/listiry/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Oct 2018 14:59:02 GMT -->
</html>
