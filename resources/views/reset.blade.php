@include('header')
<style>
    .site-header{
        background: #000;
    }
    .main-navigation > li > a,.sign-in{
        color: #fff;
    }
    .main-navigation > li > a:hover,.main-navigation > li > a.active,.sign-in:hover{
        color: #00a0e1;
    }
    .page-content{
        padding: 50px 0px;
    }
</style>
<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <div class="form-login js-login-form">
                    <div class="form-login__block js-form-block" id="reset">
                        <form action="http://haintheme.com/" method="post">
                            <div class="form-container">
                                <div class="form-group">
                                    <label for="reset-password">Email</label>
                                    <input
                                            type="email"
                                            name="reset-password"
                                            id="reset-password"
                                            class="form-input form-input--pill form-input--border-c-gallery"
                                            placeholder="johndoe@gmail.com"
                                            required
                                    >
                                </div><!-- .form-group -->

                                <div class="form-group--submit">
                                    <button
                                            class="button button--primary button--pill button--large button--block button--submit"
                                            type="submit"
                                    >
                                        Envoyer
                                    </button>
                                </div>

                                <div class="form-group--footer">
                                    <a href="#signin" id="btnlogin" class="c-secondary t-underline js-block-trigger">Retour à la connexion</a>
                                </div>
                            </div><!-- .form-container -->
                        </form><!-- .reset -->
                    </div><!-- .form-login__block -->
                </div><!-- .form-login -->
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .page-content -->
@include('footer')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4"></script>
<script src="../../../../cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
<script src="assets/scripts/app.js"></script>
</body>
<!-- Mirrored from haintheme.com/demo/html/listiry/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Oct 2018 14:59:02 GMT -->
</html>
