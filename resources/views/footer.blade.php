
<footer id="colophone" class="site-footer">
    <div class="t-center site-footer__primary">
        <div class="container">
            <div class="row">
                <center style="width: 100%;margin-bottom: 30px">
                    <img src="assets/images/logo-1.png">
                </center>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <ul class="Footer-menu">
                        <li class="Footer-item"><a href="/qui-sommes-nous" class="Footer-link">Qui sommes-nous ?</a></li>
                        <li class="Footer-item"><a href="/emploi" class="Footer-link">Rejoignez notre équipe</a></li>
                        <li class="Footer-item"><a href="/entreprises-collectivites" class="Footer-link">Comment ça marche</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="Footer-menu">
                        <li class="Footer-item"><a href="/faq" class="Footer-link">Aide / FAQ</a></li>
                        <li class="Footer-item"><a href="https://123envoiture.zendesk.com/hc/fr/requests/new" class="Footer-link">Contactez-nous</a>
                        </li>
                        <li class="Footer-item Footer-item--last">
                            <a href="https://lp.idvroom.com/bonne-id/prog-fid.html" class="Footer-link">Nos partenaires</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="Footer-menu">
                        <li class="Footer-item"><a href="/conditions-generales-utilisation" class="Footer-link">Conditions générales</a></li>
                        <li class="Footer-item"><a href="/mentions-legales" class="Footer-link">Mentions légales</a></li>
                        <li class="Footer-item"><a href="/charte-de-confidentialite" class="Footer-link">Charte de confidentialité</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- .site-footer__primary -->

    <div class="site-footer__secondary">
        <div class="container">
            <div class="site-footer__secondary-wrapper">
                <p class="site-footer__copyright">&copy; 2018
                    <span class="c-secondary">Drive</span> by worldevs. All Rights Reserved.</p>
                <ul class="min-list inline-list site-footer__links site-footer__social">
                    <li>
                        <a href="#">
                            <i class="fa fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-youtube"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- .site-footer__secondary -->
</footer><!-- #colophone -->