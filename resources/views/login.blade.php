@include('header')
<style>
    .site-header{
        background: #000;
    }
    .main-navigation > li > a,.sign-in{
        color: #fff;
    }
    .main-navigation > li > a:hover,.main-navigation > li > a.active,.sign-in:hover{
        color: #00a0e1;
    }
    .page-content{
        padding: 50px 0px;
    }
</style>
<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-md-4">
                <div class="form-login js-login-form">
                    <div class="form-login__block js-form-block is-selected" id="signin">
                        <form action="auth/login" method="post">
                            <div class="form-container">
                                <h3 class="form-title t-center">Se connecter</h3>
                                <div class="form-social">
                                    <div class="form-group">
                                        <div class="form-group__wrapper">
                                            <button
                                                    class="button button--social button--twitter button--pill button--large button--block"
                                                    type="button"
                                            >
                                                Se connecter avec Twitter
                                            </button>
                                            <span class="form-group__icon form-group__icon--social">
                  <i class="fa fa-twitter c-white"></i>
                </span>
                                        </div><!-- .form-group__wrapper -->
                                    </div><!-- .form-group -->

                                    <div class="form-group">
                                        <div class="form-group__wrapper">
                                            <button
                                                    class="button button--social button--facebook button--pill button--large button--block"
                                                    type="button"
                                            >
                                                Se connecter avec Facebook
                                            </button>
                                            <span class="form-group__icon form-group__icon--social">
                  <i class="fa fa-facebook-f c-white"></i>
                </span>
                                        </div><!-- .form-group__wrapper -->
                                    </div><!-- .form-group -->
                                </div><!-- .form-social -->
                                <div class="form-group">
                                    <label for="login-user">Email ou Téléphone</label>
                                    <input
                                            type="text"
                                            name="email"
                                            id="login-user"
                                            class="form-input form-input--pill form-input--border-c-gallery"
                                            required
                                            placeholder="johndoe"
                                    >
                                </div><!-- .form-group -->

                                <div class="form-group">
                                    <label for="login-password">Mot de passe</label>
                                    <input
                                            type="password"
                                            name="password"
                                            id="login-password"
                                            class="form-input form-input--pill form-input--border-c-gallery"
                                            required
                                            placeholder="******"
                                    >
                                </div><!-- .form-group -->

                                <div class="form-group">
                                    <div class="form-group__container">
                                        <label for="remember-me" class="icheck_label">
                                            <input type="checkbox" id="remember-me" name="iCheck">
                                            Se souvenir de moi
                                        </label>
                                        <a href="reset" id="btnreset" class="c-gray">Mot de passe oublié ?</a>
                                    </div><!-- .form-group__container -->
                                </div><!-- .form-group -->

                                <div class="form-group--submit">
                                    <button
                                            class="button button--primary button--pill button--large button--block button--submit"
                                            type="submit"
                                    >
                                        Connexion
                                    </button>
                                </div>

                                <div class="form-group--footer">
            <span class="c-gray">
              Avez-vous déjà un compte? <a href="register" class="">Créer un compte</a>
            </span>
                                </div>
                            </div><!-- .form-container -->
                        </form><!-- .signin -->
                    </div><!-- .form-login__block -->

                </div><!-- .form-login -->
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .page-content -->
@include('footer')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4"></script>
<script src="../../../../cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
<script src="assets/scripts/app.js"></script>
</body>
<!-- Mirrored from haintheme.com/demo/html/listiry/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Oct 2018 14:59:02 GMT -->
</html>
