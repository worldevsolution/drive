
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from haintheme.com/demo/html/listiry/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Oct 2018 14:56:35 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Drive</title>
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,600" rel="stylesheet">
    <link rel="stylesheet" href="assets/styles/style.css">
</head>
<style>
    .form-login__block{
        display: block;
    }
    .Footer-menu .Footer-item{
        list-style: none;
        text-align: left;
        padding: 10px 0px;
        border-bottom:1px solid rgba(204, 204, 204, 0.29);
    }
    .Footer-menu .Footer-item a{
        color: white;
        padding: 10px 0px;
    }
    .Footer-menu .Footer-item a:hover{
        color: #00a0e1;
        font-size: 14.5px;
    }
    .site-footer__primary{
        padding: 40px 0px;
    }
    .main-navigation li a{
        font-size: 17px;
    }
    .main-navigation li .fa{

    }
    .button--primary:hover{
        color: #fff;
    }
</style>
<body>
<header id="masthead" class="site-header site-header--layout-2">
    <div class="container">
        <div class="d-lg-flex justify-content-lg-between align-items-lg-center site-header__container">
            <div class="site-header__logo">
                <a href="/">
                    <h1 class="screen-reader-text">Welcome</h1>
                    <img src="assets/images/logo-2.png" alt="Drive">
                </a>
            </div>
            <!-- .site-header__logo -->
            <div class="d-lg-flex align-items-lg-center">
                <ul class="min-list main-navigation">
                    <li>
                        <a href="search"><i class="fa fa-search"></i>&nbsp;&nbsp;Rechercher</a>
                    </li>
                    <li>
                        <a href="add_trajet"><i class="fa fa-plus"></i>&nbsp;&nbsp;Proposer un trajet</a>
                    </li>
                </ul><!-- .main-navigation -->

                <div class="user-action" style="padding-left: 0px;">
                    <a href="register" class="sign-in" style="margin-left: 15px;font-size:17px">
                        Inscription
                    </a>
                    <button style="font-size: 17px" class="button button--small button--square button--primary d-none d-lg-inline-block" onclick="window.location='login'"><i class="fa fa-user"></i>&nbsp;&nbsp;Connexion</button>
                </div>
            </div>

            <div class="d-lg-none nav-mobile">
                <a href="#" class="nav-toggle js-nav-toggle nav-toggle--dove-gray">
                    <span></span>
                </a>
                <!-- .nav-toggle -->
            </div>
            <!-- .nav-mobile -->

        </div>
        <!-- .site-header__container -->

    </div>
    <!-- .container -->
</header>
