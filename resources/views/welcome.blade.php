@include('header')

<style>
    .main-search--layout-1{
        max-width: 1000px;
    }
</style>
<!-- #masthead -->
<section class="page-banner page-banner--layout-2 parallax" style="padding: 150px 0;">
    <div class="container">
        <div class="page-banner__container animated fadeInUp">
            <div class="page-banner__textcontent t-center">
                <h2 class="page-banner__title c-white">Et vous qui allez vous retrouver ?</h2>
                <p class="page-banner__subtitle c-white">Le covoiturage simplifie vos trajets.</p>
            </div><!-- .page-banner__textcontent -->

            <div class="main-search-container">
                <form class="main-search main-search--layout-1 bg-mirage" action="search">
                    <div class="main-search__group main-search__group--primary">
                        <select class="form-input">
                            <option>Départ</option>
                        </select>
                    </div><!-- .main-search__group -->

                    <div class="main-search__group main-search__group--primary">
                        <select class="form-input">
                            <option>Destination</option>
                        </select>
                    </div><!-- .main-search__group -->
                    <div class="main-search__group main-search__group--primary">
                        <input type="date" name="location" id="main-search-location" class="form-input" placeholder="Date">
                    </div><!-- .main-search__group -->
                    <div class="main-search__group main-search__group--tertiary">
                        <button type="submit" class="button button--medium button--square button--primary">
                            <i class="fa fa-search"></i> Rechercher
                        </button>
                    </div>
                </form>
            </div><!-- .main-search-container -->

        </div><!-- .page-banner__container -->
    </div><!-- .container -->
</section><!-- .page-banner -->
<section class="category-container page-section category--layout-2">
    <div class="container">
        <div class="row" style="margin-top: 40px;">
            <div class="col-md-6">
                <img src="assets/images/uploads/category-bg-5.jpg" style="width: 100%;height: 100%">
            </div>
            <div class="col-md-6">
                <h4>Vous avez une voiture?<br>
                Ne roulez pas seuls !</h4>
                <p>
                    Covoiturer c'est l'occasion de briser la routine, de faire des rencontres, de discuter ...
                    <br><br>Pour plus de convivialité vous pouvez même rejoindre la communauté de covoitureurs.</p>
                <div class="row" style="margin-top: 40px;">
                    <div class="col-md-12">
                        <a href="add_trajet" class="button button--square button--primary button--large">Proposer un trajet</a>
                    </div>
                </div>
                <br><br>
                <h4>Ce qui va vous plaire?</h4>
                <ul class="uldebut">
                    <li>Avoir le choix de milliers de destinations</li>
                    <li>Etre un membre certifié de notre large communauté</li>
                    <li>Bénéficier des avantages de nos garages partenaires</li>
                </ul>

            </div>
        </div>
    </div><!-- .container -->
</section><!-- .category -->
<style>
    .uldebut{
        padding-left: 20px;
    }
    .uldebut li{
        padding-bottom: 10px;
    }
    .effetcercle{
       border-radius: 50%;
        background: black;
        height: 150px;
        width: 150px;
        position: absolute;
        right: 15px;
        top: -20px;
        color: white;
    }
    .effetcercle h5,.effetcercle small{
        color:white;
    }
</style>
<section class="ads ads--layout-1 parallax">
    <div class="t-center ads__container">
        <div class="container">
            <h2 class="ads__title">Nos top trajets</h2>
            <div class="row" style="margin-top: 60px">
                <div class="col-md-4">
                    <div class="row" style="background: #fff;margin: 0px;">
                        <div class="col-md-6" style="text-align: left;padding: 25px 0px">
                            <div class="col-md-12" style="padding-bottom: 10px">
                                <label style="margin: 0px">Douala</label>
                            </div>
                            <div class="col-md-12">
                                <label style="margin: 0px">Yaoundé</label>
                            </div>
                        </div>
                        <div class="col-md-6 effetcercle" style="display: flex;justify-content: center;align-items: center;vertical-align: middle">
                            <div style="display: block;color:#054752">
                                <small>A partir de</small>
                                <h5 style="margin-bottom: 0px">15 000 Fcfa</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row" style="background: #fff;margin: 0px;">
                        <div class="col-md-6"  style="text-align: left;padding: 25px 0px;">
                            <div class="col-md-12" style="padding-bottom: 10px">
                                <label style="margin: 0px">Douala</label>
                            </div>
                            <div class="col-md-12">
                                <label style="margin: 0px">Kribi</label>
                            </div>
                        </div>
                        <div class="col-md-6 effetcercle" style="display: flex;justify-content: center;align-items: center;vertical-align: middle">
                            <div style="display: block;color:#054752">
                                <small>A partir de</small>
                                <h5 style="margin-bottom: 0px">6 000 Fcfa</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row" style="background: #fff;margin: 0px;">
                        <div class="col-md-6" style="text-align: left;padding: 25px 0px;">
                            <div class="col-md-12" style="padding-bottom: 10px">
                                <label style="margin: 0px">Douala</label>
                            </div>
                            <div class="col-md-12">
                                <label style="margin: 0px">Bafoussam</label>
                            </div>
                        </div>
                        <div class="col-md-6 effetcercle" style="display: flex;justify-content: center;align-items: center;vertical-align: middle">
                            <div style="display: block;color:#054752">
                                <small>A partir de</small>
                                <h5 style="margin-bottom: 0px">9 000 Fcfa</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 40px;">
                <div class="col-md-12">
                    <center><a href="toptrajet" class="button button--square button--primary button--large">Voir plus de top trajets</a></center>
                </div>
            </div>
        </div>
    </div>
</section><!-- .ads -->
<style>
    .col-md-4 .row:first-child{
        border-top-right-radius: 46%;
        border-bottom-right-radius: 46%;
    }
    .col-md-12 label{
        font-size:20px;
    }
    .col-md-6 small{
        font-size:16px;
    }
</style>
<section class="instructions-container page-section instruction--layout-2" style="margin-top: 70px">
  <div class="container">
    <h2 class="page-section__title t-center">Comment ça marche ?</h2>
    <div class="instructions">
      <div class="instruction-container">
        <div class="instruction t-center">
          <div class="instruction__image">
            <i class="fa fa-user-plus" style="font-size: 60px;"></i>
          </div>
          <h3 class="instruction__title">S'inscrire</h3>
          <p class="instruction__detail c-boulder">
            Remplissez le formulaire et devenez membre de notre communauté.
          </p>
        </div><!-- .instruction -->
      </div><!-- .col -->

      <div class="instruction-container">
        <div class="instruction t-center">
          <div class="instruction__image">
              <i class="fa fa-search" style="font-size: 60px;"></i>
          </div>
          <h3 class="instruction__title">Rechercher un trajet</h3>
          <p class="instruction__detail c-boulder">
            Retrouvez un trajet parmi nos milliers de trajets et de destinations
          </p>
        </div><!-- .instruction -->
      </div><!-- .col -->

      <div class="instruction-container">
        <div class="instruction t-center">
          <div class="instruction__image">
              <i class="fa fa-calendar" style="font-size: 60px;"></i>
          </div>
          <h3 class="instruction__title">Réserver un trajet</h3>
          <p class="instruction__detail c-boulder">
            Prenez place dans une voiture et voyagez en bonne compagnie
          </p>
        </div><!-- .instruction -->
      </div><!-- .col -->
    </div><!-- .instructions -->
  </div><!-- .container -->
</section><!-- .instruction-container -->
<section style="background: #00a0e1;padding: 0px; margin-bottom: 80px; height: 200px;">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <img src="assets/images/phone.png" style="width:272px;position: absolute;top: -50px;">
            </div>
            <div class="col-md-7" style="height: 226px;margin-top: 5%;">
                <h4 style="color: #fff;font-weight: 100;font-size: 1.25rem;text-align: center">Retrouvez tous nos services sur l’application mobile</h4>
                <center style="display: flex;justify-content: center;align-items: center;vertical-align: middle;"><img src="assets/images/apple.png" style="width: 140px;height: 45px;">&nbsp;&nbsp;&nbsp;
                    <img src="assets/images/google.png" style="width: 140px;height: 45px;"></center>

            </div>
        </div>
    </div>
</section>
<section class="page-section testimonials-container testimonial--layout-2" style="background: #f6f6f6;">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="testimonial-video">
                    <img src="assets/images/uploads/testimonial-video.png" alt="Testimonial Thumbnail">
                    <a href="https://www.youtube.com/watch?v=20bpjtCbCz0" data-lity>
                        <i class="fa fa-play-circle testimonial-video-icon"></i>
                    </a>
                </div>
            </div><!-- .col -->

            <div class="col-md-8">
                <div class="testimonials">
                    <h2 class="page-section__title">Témoignages des covoitureurs</h2>
                    <div class="swiper-container testimonial-container">
                        <div class="swiper-wrapper testimonial-wrapper">
                            <div class="swiper-slide testimonial">
                                <p class="testimonial-content c-dove-gray">
                                    Mitch and I have traveled all over but this was by far one of the best vacations I have been on.
                                    Mitch will be writing you about future trips. Make sure Mitch books business class or first class in the future.
                                </p>
                                <div class="testimonial-footer">
                                    <div class="testimonial-avatar">
                                        <img src="assets/images/uploads/testimonial-avatar.png"
                                             class="Client Avatar"
                                             alt="Testimonial Image"
                                        >
                                    </div>

                                    <div class="testimonial-client">
                                        <span class="testimonial-client-name">- Roberta V</span>
                                        <span class="testimonial-client-location">India</span>
                                    </div>
                                </div><!-- .testimonial-footer -->
                            </div><!-- .testimonial -->

                            <div class="swiper-slide testimonial">
                                <p class="testimonial-content c-dove-gray">
                                    Mitch and I have traveled all over but this was by far one of the best vacations I have been on.
                                    Mitch will be writing you about future trips. Make sure Mitch books business class or first class in the future.
                                </p>
                                <div class="testimonial-footer">
                                    <div class="testimonial-avatar">
                                        <img src="assets/images/uploads/testimonial-avatar.png"
                                             class="Client Avatar"
                                             alt="Testimonial Image"
                                        >
                                    </div>

                                    <div class="testimonial-client">
                                        <span class="testimonial-client-name">- Roberta V</span>
                                        <span class="testimonial-client-location">India</span>
                                    </div>
                                </div><!-- .testimonial-footer -->
                            </div><!-- .testimonial -->

                            <div class="swiper-slide testimonial">
                                <p class="testimonial-content c-dove-gray">
                                    Mitch and I have traveled all over but this was by far one of the best vacations I have been on.
                                    Mitch will be writing you about future trips. Make sure Mitch books business class or first class in the future.
                                </p>
                                <div class="testimonial-footer">
                                    <div class="testimonial-avatar">
                                        <img src="assets/images/uploads/testimonial-avatar.png"
                                             class="Client Avatar"
                                             alt="Testimonial Image"
                                        >
                                    </div>

                                    <div class="testimonial-client">
                                        <span class="testimonial-client-name">- Roberta V</span>
                                        <span class="testimonial-client-location">India</span>
                                    </div>
                                </div><!-- .testimonial-footer -->
                            </div><!-- .testimonial -->
                        </div><!-- .testimonial-wrapper -->

                        <div class="testimonial-button-container">
                            <span class="ion-chevron-left testimonial-button testimonial-button-left"></span>
                            <span class="ion-chevron-right testimonial-button testimonial-button-right"></span>
                        </div>
                    </div><!-- .testimonial-container -->
                </div><!-- .testimonials -->
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</section><!-- .testimonial -->

<section class="ads ads--layout-1 parallax">
    <div class="t-center ads__container">
        <div class="container">
            <h2 class="ads__title">Devenir garage partenaire</h2>
            <p class="ads__subtitle">
                Preparing for your traveling is very important. Our book store has lots of e-books, it might be helpful for you
            </p>
            <br>
            <br>
            <a href="#modalPartenaire" data-toggle="modal" class="button button--square button--primary button--large">Nous contacter</a>
        </div>
    </div>
</section><!-- .ads -->
<section class="news page-section news--layout-2">
    <div class="container">
        <h2 class="page-section__title t-center">Le covoiturage selon drive</h2>
        <div class="row">
            <div class="col-md-4">
                <div class="post sticky">
                    <div class="post-thumbnail">
                        <a href="single.html">
                            <img src="assets/images/uploads/news-4.jpg" alt="Sa Pa festival for next month">
                        </a>
                    </div><!-- .post-thumbnail -->

                    <div class="post-content bg-wild-sand">
                        <h3 class="post-title">
                            <a href="single.html">Sa Pa festival for next month</a>
                        </h3>
                        <p class="post-excerpt c-dove-gray">
                            Mirum est notare quam littera gothica, quam nunc putamus parum claram,
                            anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.
                        </p>
                        <p class="post-meta t-small">
                            <span><a href="#" class="c-dusty-gray">Dec 05, 2017</a></span>
                            <span><a href="#" class="c-dusty-gray">Restaurant</a></span>
                        </p>
                    </div><!-- .post-content -->
                </div><!-- .post -->
            </div><!-- .col -->

            <div class="col-md-4">
                <div class="post">
                    <div class="post-thumbnail">
                        <a href="single.html">
                            <img src="assets/images/uploads/news-5.jpg" alt="Summer Skiing: 16 Sweetest Summer Ski Resorts">
                        </a>
                    </div><!-- .post-thumbnail -->

                    <div class="post-content bg-wild-sand">
                        <h3 class="post-title">
                            <a href="single.html">16 Sweetest Summer Ski Resorts</a>
                        </h3>
                        <p class="post-excerpt c-dove-gray">
                            Mirum est notare quam littera gothica, quam nunc putamus parum claram,
                            anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.
                        </p>
                        <p class="post-meta t-small">
                            <span><a href="#" class="c-dusty-gray">Dec 05, 2017</a></span>
                            <span><a href="#" class="c-dusty-gray">Travel</a></span>
                        </p>
                    </div><!-- .post-content -->
                </div><!-- .post -->
            </div><!-- .col -->

            <div class="col-md-4">
                <div class="post">
                    <div class="post-thumbnail">
                        <a href="single.html">
                            <img src="assets/images/uploads/news-6.jpg" alt="Best destination for travelers">
                        </a>
                    </div><!-- .post-thumbnail -->

                    <div class="post-content bg-wild-sand">
                        <h3 class="post-title">
                            <a href="single.html">Best destination for travelers</a>
                        </h3>
                        <p class="post-excerpt c-dove-gray">
                            Mirum est notare quam littera gothica, quam nunc putamus parum claram,
                            anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.
                        </p>
                        <p class="post-meta t-small">
                            <span><a href="#" class="c-dusty-gray">Dec 05, 2017</a></span>
                            <span><a href="#" class="c-dusty-gray">Culture</a></span>
                        </p>
                    </div><!-- .post-content -->
                </div><!-- .post -->
            </div><!-- .col -->
        </div><!-- .row -->
    </div><!-- .container -->
</section><!-- .news -->
@include('footer')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDyCxHyc8z9gMA5IlipXpt0c33Ajzqix4"></script>
<script src="../../../../cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
<script src="assets/scripts/app.js"></script>
</body>

<!-- Mirrored from haintheme.com/demo/html/listiry/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Oct 2018 14:57:33 GMT -->
</html>