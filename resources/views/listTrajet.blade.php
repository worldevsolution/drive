@include('header')
    <style>
    .site-header{
    background: #000;
}
    .main-navigation > li > a,.sign-in{
    color: #fff;
}
    .main-navigation > li > a:hover,.main-navigation > li > a.active,.sign-in:hover{
    color: #00a0e1;
}
    .page-content{
    padding: 50px 0px;
    }
    .row .col-md-4 div{
        background: #efecec;
        padding: 20px;
    }
    .row .col-md-4 div ul{
        margin-left: 20px;
        list-style: none;
    }
    .row .col-md-4 div ul li{
        padding:8px 0px;
    }
    .row .col-md-4 div:hover{
        box-shadow: 3px 2px 1px 1px #a0a0a0;
    }
</style>
<div class="page-content">
    <div class="container">
        <div class="row" style="margin-top: 30px;">
            <div class="col-md-12">
                <h4>Rechercher un trajet en covoiturage</h4>
                <h6>Vous recherchez un covoiturage ? Pour aider, les voyageurs, Drive vous propose un listing des principaux covoiturages.</h6>
            </div>
            <div class="row" style="width: 100%;margin-top: 30px;">
                <div class="col-md-4">
                    <div class="background:white">
                        <h5>Au départ de douala</h5>
                        <ul>
                            <li>
                                Douala - Yaoundé
                            </li>
                            <li>
                                Douala - Bafoussam
                            </li>
                            <li>
                                Douala - Ngaoundéré
                            </li>
                            <li>
                                Douala - Garoua
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="background:white">
                        <h5>Au départ de Yaoundé</h5>
                        <ul>
                            <li>
                                Yaoundé - Douala
                            </li>
                            <li>
                                Yaoundé - Bafoussam
                            </li>
                            <li>
                                Yaoundé - Ngaoundéré
                            </li>
                            <li>
                                Yaoundé - Garoua
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="background:white">
                        <h5>Au départ de Bafoussam</h5>
                        <ul>
                            <li>
                                Bafoussam - Douala
                            </li>
                            <li>
                                Bafoussam - Yaoundé
                            </li>
                            <li>
                                Bafoussam - Ngaoundéré
                            </li>
                            <li>
                                Bafoussam - Garoua
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .page-content -->
@include('footer')