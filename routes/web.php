<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function () {
    return view('login');
});


Route::post('auth/login', 'Auth\LoginController@login');

Route::get('/reset', function () {
    return view('reset');
});

Route::get('/toptrajet', function () {
    /*$endpoint = 'http://127.0.0.1:8000/api';
    $client = new \GuzzleHttp\Client();
    // Create a request
    $request = $client->get($endpoint.'/point');*/
    return view('listTrajet');
});

Route::get('/search', function () {
    return view('recherche');
});

Route::get('/add_trajet', function () {
    return view('addTrajet');
});

Route::get('/register', function () {
    return view('register');
});
